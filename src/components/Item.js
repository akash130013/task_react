import React from 'react'

export default function Item({ index, item }) {

    return (

        <div className="item">

            <div className="content">
                <div className="description">
                    <a className="ui black circular label">{index + 1}</a>

                    {item}
                </div>
            </div>
        </div>

    )
}
