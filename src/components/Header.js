import React from 'react'
import { Link } from 'react-router-dom'

export default function Header() {
    return (
        <>
            <div className="ui clearing segment">

            <h3 className="ui left floated header">
                  <Link to="/">  Form Validation Task</Link>
                </h3>
                <h3 className="ui right floated header">
                   <Link to="/todo"> To Do Task</Link>
                </h3>
            </div>
        </>
    )
}
