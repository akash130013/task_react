import React from 'react'
import { useState } from 'react'

export default function TodoItem({todoitem}) {

    const [todo, setTodo] = useState('');
    const [error, setError] = useState(false);

    let handleChange = (e) => {
        setTodo(e.target.value);
    }


    let validate = () => {
        let input = todo;
        let isValid = true;
        if (input.length == 0) {
            setError(true);
            isValid = false;
        }
        return isValid;
    }

    let handleSubmit = () => {
        if (validate()) {
            setError(false);
            setTodo('');
           todoitem(todo);

        }
    }
    return (
        <>
        <div className="ui input focus">
            <input placeholder="Type something here..." onChange={handleChange} value={todo} /> 
            <button className="ui button" type="button" onClick={handleSubmit} >Add</button>
        </div>
        {error && <div className="ui warning message"> Please add something </div> }
        
      
     </>
    )
}
