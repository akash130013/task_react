import React, { Component } from 'react'
import TodoItem from './TodoItem'
import Item from './Item'

export default class Todolist extends Component {


    state = {
        todo: [],
    }

    /**
     * 
     * @param {*} item 
     * @description add to do item and set state
     */
    addTodo = (item) => {
        let data = [...this.state.todo, item]

        this.setState({
            todo: data,
        })
    }

    render() {

        return (
            <>
                <TodoItem todoitem={this.addTodo} />
                <div className="ui relaxed divided list">

                    {this.state.todo.map((item, index) => {
                        return <Item item={item} key={index} index={index} />
                    })}

                </div>
            </>
        )
    }
}
