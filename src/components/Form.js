import React from 'react';

class Form extends React.Component {


    constructor() {
        super();
        this.state = {
            input: {},
            errors: {}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    /**
     * 
     * @param {*} event 
     * @description hadle input change
     */
    handleChange(event) {
        let input = this.state.input;
        input[event.target.name] = event.target.value;

        this.setState({
            input
        });
    }

    /**
     * 
     * @param {*} event 
     * @description form submit event and check if it valid
     */
    handleSubmit(event) {
        event.preventDefault();

        if (this.validate()) {
          
            let input = {};

            input["email"] = "";
            input["password"] = "";
            this.setState({ input: input });

            alert('Form is submitted');
            return false;
        }
    }

    /**
     * @description validation of all the input
     */
    validate() {
        let input = this.state.input;
        let errors = {};
        let isValid = true;

        if (!input["email"]) {
            isValid = false;
            errors["email"] = "Please enter your email Address.";
        }

        if (typeof input["email"] !== "undefined") {

            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(input["email"])) {
                isValid = false;
                errors["email"] = "Please enter valid email address.";
            }
        }

        if (!input["password"]) {
            isValid = false;
            errors["password"] = "Please enter your password.";
        }



        if (typeof input["password"] !== "undefined") {
            if (input["password"].length < 6) {
                isValid = false;
                errors["password"] = "Please add at least 6 charachter.";
            }
        }


        this.setState({
            errors: errors
        });

        return isValid;
    }

    render() {
        return (
            <>

                <form onSubmit={this.handleSubmit} className="ui form">

                    <div className="field">
                        <label htmlFor="email">Email Address:</label>
                        <input
                            type="text"
                            name="email"
                            value={this.state.input.email || ''}
                            onChange={this.handleChange}
                            placeholder="Enter email"
                            autoComplete="off"
                            id="email" />


                        {this.state.errors.email &&
                            <div className="ui tiny message">
                                {this.state.errors.email}
                            </div>
                        }
                    </div>

                    <div className="field">
                        <label htmlFor="password">Password:</label>
                        <input
                            type="password"
                            name="password"
                            value={this.state.input.password || ''}
                            onChange={this.handleChange}
                            placeholder="Enter password"
                            id="password" />
                        {this.state.errors.password &&
                            <div className="ui tiny message">
                                {this.state.errors.password}
                            </div>
                        }
                    </div>
                    <input type="submit" value="Submit" className="ui button" />
                </form>
            </>
        );
    }
}

export default Form;


