import React, { Component } from 'react'

import { Router, Route, Switch } from 'react-router-dom'
import History from './History'
import Header from './components/Header'
import Form from './components/Form'
import Todolist from './components/Todolist'

export default class App extends Component {

    render() {
        return (
            <div className="ui container">
                <Router history={History}>
                    <Header />
                    <Switch>
                        <Route path='/' exact={true} component={Form} />
                        <Route path='/todo' component={Todolist} />
                    </Switch>
                </Router>

            </div>
        )
    }
}
